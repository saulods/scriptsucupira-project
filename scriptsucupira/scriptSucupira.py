#!/usr/bin/python
# -*- coding: utf-8 -*-
##################################################
# ScriptSucupira
# Authors: Wonder Alexandre Luz Alves
#          Luc Quoniam
# Dates:
#   - Version 1.0 (25/04/2014) - first version
#   - Version 1.1 (29/11/2014) - bug fixes
#   - Version 1.2 (19/01/2015) - n-qualis
#   - Version 1.3 (12/08/2015) - bug fixes (duplicate members, html, incompatible period in LP and PR)....
##################################################.


import csv
import sys
import os, errno, copy, shutil
import datetime 
import string
import operator
from unicodedata import normalize
from collections import namedtuple
from operator import contains


##
# Um objeto desta classe representa a estatistica gerada por uma execucao.
#
class Estatistica:
    
    # @param person: Who should think.
    # @type person: L{Person} or L{Animal}
    # @param time: How long they should think.
    # @type time: C{int} or C{float}
    
    
    contAluno = None
    contProfessor = None
    contLinhaPesquisa = None
    contProgramaComAlunos = None
    contProgramasComAlunos = None
    contPrograma = None
    contProgramas = None
    contProgramaDatas = None
    contProgramasDatas = None

    def __init__(self):
        self.contAluno = 0
        self.contProfessor = 0
        self.contLinhaPesquisa = 0
        self.contProgramaComAlunos = 0
        self.contProgramasComAlunos = 0
        self.contPrograma = 0
        self.contProgramas = 0
        self.contProgramaDatas = 0
        self.contProgramasDatas = 0

    def getTotal(self):
        return self.contAluno + self.contProfessor + self.contLinhaPesquisa + self.contProgramaComAlunos + self.contProgramasComAlunos + self.contPrograma + self.contProgramas + self.contProgramaDatas + self.contProgramasDatas


def removerCaracteresEspeciais(txt, codif='utf-8'):
    return normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore').replace(" ", "-")



##
# Um objeto desta classe representa um curriculo lattes (aluno ou professor)
#
class CvLattesProfessor: 
    fields = None
    periods = []
    periodoInicial = 99999
    periodoFinal = 0
    
    def __init__(self, fields):
        self.fields = fields
        for p in self.fields:
            if str(p).startswith("Periodo_Inicio_"): 
                if len(self.getField(p)) > 0:
                    self.periodoInicial = min(eval(self.getField(p)), self.periodoInicial )
            if str(p).startswith("Periodo_Fim_"):
                if len(self.getField(p)) > 0:    
                    self.periodoFinal = max(eval(self.getField(p)), self.periodoFinal )
        
        year = self.periodoInicial
        while year <= self.periodoFinal:
            self.periods = self.periods + [year]
            year = year + 1
    
    
    def getCvComPeriodoAjustado(self, prog):
        c = CvLattesProfessor(self.fields)
        index = prog.partition("_")[2] ## TODO: VERIFICAR! NAO ENTENDI O QUE ISSO FAZ
        c.periodoInicial = eval(self.getField("Periodo_Inicio_" + str(index)))
        c.periodoFinal = eval(self.getField("Periodo_Fim_" + str(index)))
        return c
    
    def getPeriodoInicial(self):
        return str(self.periodoInicial)
    
    def getPeriodoFinal(self):
        return str(self.periodoFinal)
    
    def getPeriodoInicialPorPrograma(self, prog):
        index = prog.partition("_")[2]
        return str( self.getField("Periodo_Inicio_" + str(index)) )
    
    def getPeriodoFinalPorPrograma(self, prog):
        index = prog.partition("_")[2]
        return str( self.getField("Periodo_Fim_" + str(index)) )
    
    
    
    #def isValidPeriodoParaPrograma(self, prog, periodo):
    #    index = None
    #    for p in self.fields:
    #        if str(p).startswith("PROGRAMA_"):
    #            if(self.getField(p) == prog ):
    #                index = p.partition("_")[2]
    #        
    #        
    #    if(index == None):
    #        return 0
    #    else:
    #        if (self.getField("Periodo_Inicio_" + str(index)) <= periodo and periodo <= self.getField("Periodo_Fim_" + str(index)) ):
    #            return 1
    #    return 0
    
    def __eq__(self, obj):
        return self.getField("ID_Lattes") == obj.getField("ID_Lattes")
    
    def hasValueInField(self, field, value):
        return self.fields[field] == value
    
    
    def getField(self, f):
        return str(self.fields[f]).strip()
    
    def getHeader(self):
        return ['ID_Lattes', 'MATRICULA', 'NOME', 'CH', 'LP_1', 'LP_2', 'LP_3', 'PROGRAMA_1', 'CATEG_1', 'PROGRAMA_2', 'CATEG_2', 'PROGRAMA_3', 'CATEG_3', 'Periodo_Inicio', 'Periodo_Fim', 'mail', 'Resp_prog', 'Prog_linha', 'Resp_LP1', 'Resp_LP2', 'Resp_LP3', 'Resp_LP4']    
    
    
    def __str__(self):        
        s = "CV["+str(self.type)+"][ID_Lattes:"+str(self.fields["ID_Lattes"])+ "; NOME:" + str(self.fields["NOME"]) + "; PROGRAMA_1:" + str(self.fields["PROGRAMA_1"]) + "]"
        return s        

class CvLattesAluno: 
    fields = None
    programasAluno = None
    
    def __init__(self, fields):
        self.fields = fields
        self.programasAluno = []
        self.anosAluno = []
        for p in self.fields:
            if str(p).startswith("PROGRAMA_"):
                self.programasAluno = self.programasAluno + [p]
                                
    def hasValueInField(self, field, value):
        return self.fields[field] == value
    
    def getField(self, f):
        return str(self.fields[f]).strip()
    
    
    def getAno(self, nameProg):
        anoMaior = 0
        for prog in self.programasAluno:
            if len(str(prog)) > 0:
                ano = self.getField("ANO_" + prog.partition("_")[2]).strip()
                if anoMaior < ano:
                    anoMaior = ano 
            if nameProg in str(self.getField(prog)):
                return self.getField("ANO_" + prog.partition("_")[2]).strip()
            
        return anoMaior
                
    def getHeader(self):
        return ['ID_Lattes','NOME','ANO_1','PROGRAMA_1','ANO_2','PROGRAMA_2']
                
    def __str__(self):        
        s = "CV["+str(self.type)+"][ID_Lattes:"+str(self.fields["ID_Lattes"])+ "; NOME:" + str(self.fields["NOME"]) + "; PROGRAMA_1:" + str(self.fields["PROGRAMA_1"]) + "]"
        return s        


##
# Um objeto desta classe representa um node de uma hierarquia de programas
#        
class NodeTree:
    
    name = None
    nameDesc = None
    isLeaf = None
    email = None
    programas = None #Ex: MPA, M-PPGA, D-PPGA (usado para associar alunos)  
    qualisCSV = "#"
    qualisPDF = "#"
    areaQualis = "#"
    
    listCvLattesProfessor = []
    listCvLattesAlunosD = []
    listCvLattesAlunosM = []
    listCvLattesAlunosMP = []
    listCvLattesAlunos = []    
    #finalYear = 0
    #initialYear = 999999
    
    key = None
    keyParent = ''
    children = []
    parent = None
    
    
    def __init__(self, key):
        self.key = key
        
    def addChild(self, son):
        self.children = self.children + [son]
        
    def __str__(self):        
        s = "Node[key:"+str(self.key)+"; keyParent:" + str(self.keyParent) 
        return s
    
    def hasCvInPeriod(self, period):
        for cv in self.listCvLattesProfessor:
            if(eval(period) in cv.periods):
            #if(str(cv.periodoInicial) ==  str(period)):
                return True
        return False    
    
    def addCvAluno(self, cv, nivel):
        #print "Adicionou mais 1"
        self.listCvLattesAlunos = self.listCvLattesAlunos + [cv]
        if nivel == "D":
            self.listCvLattesAlunosD = self.listCvLattesAlunosD + [cv]    
        elif nivel == "M":
            self.listCvLattesAlunosM = self.listCvLattesAlunosM + [cv]
        elif nivel == "MP":            
            self.listCvLattesAlunosMP = self.listCvLattesAlunosMP + [cv]
            
    def addCv(self, cv):
        self.listCvLattesProfessor = self.listCvLattesProfessor + [cv]
        #self.initialYear = min(self.initialYear,     min(cv.periods))
        #self.finalYear = max(self.finalYear,     max(cv.periods))
            
    def getAllListCvLattesAluno(self):
        #return (self.listCvLattesAlunos)
        return list(set(self.listCvLattesAlunos))
            
    def completeCV(self, node = None):
        if node == None:
            node = self
            
        for son in node.children:
            for cv in son.listCvLattesProfessor:         
                if not cv in self.listCvLattesProfessor:
                    self.addCv(cv)
                else: 
                    for cvNode in self.listCvLattesProfessor:
                        if(cvNode == cv):
                            cvNode.periodoInicial = min(cvNode.periodoInicial, cv.periodoInicial)
                            cvNode.periodoFinal = max(cvNode.periodoFinal, cv.periodoFinal)
                    
            for cv in son.listCvLattesAlunosD:        
                if not cv in self.listCvLattesAlunosD:
                    self.listCvLattesAlunosD = self.listCvLattesAlunosD + [cv]
                    if not cv in self.listCvLattesAlunos:
                        self.listCvLattesAlunos = self.listCvLattesAlunos + [cv]
            for cv in son.listCvLattesAlunosM:    
                if not cv in self.listCvLattesAlunosM:
                    self.listCvLattesAlunosM = self.listCvLattesAlunosM + [cv]
                    if not cv in self.listCvLattesAlunos:
                        self.listCvLattesAlunos = self.listCvLattesAlunos + [cv]
            for cv in son.listCvLattesAlunosMP:
                if not cv in self.listCvLattesAlunosMP:
                    self.listCvLattesAlunosMP = self.listCvLattesAlunosMP + [cv]
                    if not cv in self.listCvLattesAlunos:
                        self.listCvLattesAlunos = self.listCvLattesAlunos + [cv]
                            
            self.completeCV(son)     
                
                
##
# Um objeto desta classe representa UMA hierarquia de programas (config)
#        
class Tree:
    root = None    
    mapsNodes = None
    numNode = None
    listNodeByQualis = [] 
     
    def __init__(self, dirPath, pathComp, listLattes, listLattesA, cvsProgramas):
        self.root = NodeTree("1")
        self.mapsNodes = dict([(self.root.key, self.root)])
        self.numNode = 1
        
        compFile = open(pathComp, 'rb')    
        for row in compFile:
            row = row.strip()
            if row.startswith("Programa_") :#7.1.2
                key = row.split(':')[0].strip()
                value = row.split(':')[1].strip()
                key = key[key.rfind("_")+1: len(key)] ##TODO REVER como faz isso
                        
                if row.find("Programa_Abreviado_") >= 0: 
                    if key != "1":
                        node = NodeTree(key)        
                        parent = self.mapsNodes[ key[0:len(key)-1] ]
                        self.addNode(node, parent)
                        node.name = value
                    else:
                        self.root.name = value
                elif row.find("Programa_Completo_") >= 0:
                    if key != "1":
                        self.mapsNodes[key].nameDesc = value
                    else:
                        self.root.nameDesc = value
                elif row.find("Programa_Alunos_") >= 0:
                    if key != "1":
                        self.mapsNodes[key].programas = value.split(',')
                    else:
                        self.root.programas = value.split(',')
                        
                elif row.find("Programa_arquivo_qualis_de_periodicos_csv_") >= 0:
                    if key != "1":
                        shutil.copy(value, dirPath)
                        self.mapsNodes[key].qualisCSV = value[value.rfind("/")+1:]
                        self.listNodeByQualis = self.listNodeByQualis + [self.mapsNodes[key]]
                    else:
                        shutil.copy(value, dirPath)
                        self.root.qualisCSV = value[value.rfind("/")+1:]
                        self.listNodeByQualis = self.listNodeByQualis + [self.mapsNodes[key]]      
               
                elif row.find("Programa_arquivo_qualis_de_periodicos_pdf_") >= 0: ##7.1.7
                    if key != "1":
                        shutil.copy(value, dirPath)
                        self.mapsNodes[key].qualisPDF = value[value.rfind("/")+1:]
                    else:
                        shutil.copy(value, dirPath)
                        self.root.qualisPDF = value[value.rfind("/")+1:]                     
        
                elif row.find("Programa_area_de_avaliacao_qualis_") >= 0:
                    if key != "1":
                        self.mapsNodes[key].areaQualis = value
                    else:
                        self.root.areaQualis = value 
                        
                        
        self.loadQualisInNodes(self.root) ## TODO 7.1.9
            
        for keyNode in sorted(self.mapsNodes): ## TODO Verificar o sorte
            node = self.mapsNodes[keyNode]
            for cv in listLattes:
                for programa in cvsProgramas:
                    if(cv.getField(programa) == node.name):
                        cvClone = cv.getCvComPeriodoAjustado(programa)
                        node.addCv( cvClone )
                        break
    
            for cv in listLattesA:
                #print str(node.name) + " ==>> " + str(node.programas) + " ==>>" + str(cv.programasAluno)
                for progKey in cv.programasAluno:
                    prog = cv.getField(progKey)
                    #print cv.getField("NOME") + ": " + prog + " => encontrar em " + str(node.programas)
                    for progNode in node.programas:
                        #print prog + " == " + str(progNode).strip()
                        if prog == str(progNode).strip() :
                            if(prog.startswith("D-")):
                                node.addCvAluno( cv, "D")
                            elif(prog.startswith("M-")):
                                node.addCvAluno( cv, "M" )
                            elif(prog.startswith("MP")):
                                node.addCvAluno( cv, "MP")       
                                
        for keyNode in sorted(self.mapsNodes):
            node = self.mapsNodes[keyNode]
            node.completeCV()
            if( len(node.children) > 0):
                node.isLeaf = 0;
            else:
                node.isLeaf = 1;     
    
    def getNumNode(self):
        return self.numNode
        
    def addNode(self, node, parent):
        node.parent = parent
        parent.addChild( node )
        self.mapsNodes[node.key] = node
        self.numNode = self.numNode + 1    
    
    def loadQualisInNodes(self, node):
        for son in node.children:
            if(son.qualisCSV == "#"):
                son.qualisCSV = node.qualisCSV
            if(son.qualisPDF == "#"):
                son.qualisPDF = node.qualisPDF
            if(son.areaQualis == "#"):
                son.qualisPDF = node.areaQualis
            self.loadQualisInNodes(son)    
                   
    
    def printTree(self):
        print u"\n------------------------------------------------------"
        print   u"|             Estatistica sobre Hierarquia            |"
        print   u"------------------------------------------------------"
        self.printNodes(self.root, "-["+ removerCaracteresEspeciais(self.root.name) +"]-")
        

    def printNodes(self, node, s):
        if( node.isLeaf == 0):
            print u"\nConjunto de programas: " + removerCaracteresEspeciais(node.name)
            print u"Num CV Professores: "+ str(len(node.listCvLattesProfessor))
            print u"Num CV Alunos de mestrado: " + str(len(node.listCvLattesAlunosM) + len(node.listCvLattesAlunosMP)) 
            print u"Num CV Alunos de doutorado: " + str(len(node.listCvLattesAlunosD))  
            print u"Total CV alunos: "+ str(len(node.listCvLattesAlunos))
            print u"Total de CVs: " + str(len(node.listCvLattesProfessor) + len(node.listCvLattesAlunos))
        else:    
            print u"\n\tProgramas: " + removerCaracteresEspeciais(node.name)
            print u"\tNum CV Professores: "+ str(len(node.listCvLattesProfessor))
            print u"\tNum CV Alunos de mestrado: " + str(len(node.listCvLattesAlunosM) + len(node.listCvLattesAlunosMP)) 
            print u"\tNum CV Alunos de doutorado: " + str(len(node.listCvLattesAlunosD))  
            print u"\tTotal CV alunos: "+ str(len(node.listCvLattesAlunos))
            print u"\tTotal de CVs: " + str(len(node.listCvLattesProfessor) + len(node.listCvLattesAlunos))

            #print ("Programa: " + removerCaracteresEspeciais(node.name) + " => [Prof="+ str(len(node.listCvLattes)) + ", AlunoM=" + str(len(node.listCvLattesAlunosM)) + ", AlunoD=" + str(len(node.listCvLattesAlunosD))  + ", Total alunos="+ str(len(node.listCvLattesAlunosM) + len(node.listCvLattesAlunosD)) +", Total de CVs="+", Total alunos="+ str(len(node.listCvLattes) + len(node.listCvLattesAlunosM) + len(node.listCvLattesAlunosD)) +"] " ) 
        for son in node.children:
            self.printNodes(son, ("--"*(len(son.key))) + "["+ removerCaracteresEspeciais(son.name) +"]"+ (len(son.key)*"--"))

    def drawNodes(self, outFile, parent, node):
        outFile.write('{"' +  parent.name + '"} -> {"'+ node.name +'"}; \n')
        for son in node.children:
            self.drawNodes(outFile, node, son)    
    
    def drawTree(self):
        outFile = open("hierarchy.dot", "wb")
        outFile.write( "digraph G { \n" )
        for son in self.root.children:
            self.drawNodes(outFile, self.root, son)    
    
        outFile.write("} \n")
        outFile.close()    
    
     
##----------------------------------------------------------------------------------------------------
# main e funcoes auxiliares
#----------------------------------------------------------------------------------------------------
class GeradorScriptlattes:
    stat = Estatistica()
    tree = None
    typeScript = "Professor"
    globaldirCache = None
    dirDOI = None
    #fileQualis = "#"
    #fileQualisPDF = "#"
    nameUniversity = None
    emailResponsavel = None
    
    htmlPaginaRaiz = None
    htmlPaginaPeridos = []
    mapPeridos = []
    titleHtml = "Stricto sensu"
    dirUniversity = None
    dirPath = "./"
    
    initialYear = 99999
    finalYear = 0
   
    fileScriptRun = None
    fileScriptRunLog = None
    
    modeloAluno = None
    modeloProfessor = None
    modeloLinhaPesquisa = None
    modeloProgramaComAlunos = None
    modeloProgramasComAlunos = None
    modeloPrograma = None
    modeloProgramas = None
    modeloProgramaDatas = None
    modeloProgramasDatas = None
    
    
    
    #----------------------------------------------------------------------------------------------------
    # funções de escrita dos arquivos configs, list, html
    #----------------------------------------------------------------------------------------------------
    def writeFilesByLinhasPesquisas(self, node,  value, cvsProgramas, cvsLinhasPesquisas, cvsRespLP):
        if(node.listCvLattesProfessor == []):
            return
        periods = []
        year = self.initialYear
        while year <= self.finalYear:
            periods = periods + [str(year)]
            year = year + 1    
        periods = periods + [str(self.initialYear) + "-" + str(self.finalYear)]
    
        listLP = []
        headLP = []
        for cv in node.listCvLattesProfessor:
            i = 0
            for p in cvsProgramas:
                if cv.getField(p) == node.name:
                    if not cv.getField(cvsLinhasPesquisas[i]) in listLP:
                        lp = cv.getField(cvsLinhasPesquisas[i])
                        if(len(lp)>0):
                            listLP = listLP + [cv.getField(cvsLinhasPesquisas[i])]
                            headLP = headLP + [cvsLinhasPesquisas[i]]
                            
                i = i + 1
            
        index = -1
        indexLP = 1
        for period in periods:    
            index = index + 1
            contLP = 0;
            for lp in listLP:
                nameLP = "LP-" + removerCaracteresEspeciais(lp).replace(" ", "_")
                cabLP = headLP[contLP]
                contLP = contLP + 1
                if(len(nameLP) > 50):
                    nameLP = nameLP[0:50] + "_" + str(indexLP)
                    indexLP = indexLP + 1
                nameFileList = self.dirPath + "config/"+period+"/" + nameLP + ".list"
                nameFileConfig = self.dirPath + "config/"+period+"/" + nameLP + ".config"
                outFileList = open(nameFileList, "wb")
                self.htmlPaginaPeridos[index].write("<h"+str(len(node.key)+1)+">"+ ("<ul>" * (len(node.key)))+ "<li><a href='"+ nameLP +"/index.html' target='_blank'>"+lp+"</a></li>"+ ("</ul>" * (len(node.key))) +"</h"+str(len(node.key)+1)+">")                
                email = self.emailResponsavel
                for cv in node.listCvLattesProfessor:
                    if len(period) == 4:
                        if (not (cv.getPeriodoInicialPorPrograma(cabLP) <= period and period <= cv.getPeriodoFinalPorPrograma(cabLP)) ):
                            continue
                    i = 0
                    flag = 0
                    for p in cvsProgramas:
                        if cv.getField(p) == node.name:
                            if cv.getField(cvsLinhasPesquisas[i]) == lp:
                                flag = 1
                            
                        i = i + 1
    
                    if flag == 0:
                        continue
                        
                    data =     None
                    if len(period) == 4:     
                        data = period + "-" + period
                    else:    
                        data = cv.getPeriodoInicialPorPrograma(cabLP) + "-" + cv.getPeriodoFinalPorPrograma(cabLP)
                    
                
                    outFileList.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ", "+data+" , \n" )
                    
                    for resp in cvsRespLP:
                        if( cv.getField(resp) == lp ):
                            email = cv.getField("mail")
        
                    
                outFileList.close()
                inFileConfig = open(self.modeloLinhaPesquisa, 'rb')
                self.stat.contLinhaPesquisa += 1
                outFileConfig = open(nameFileConfig, "wb")
                self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfig + "'  \n")
                self.fileScriptRun.write("./scriptLattes.py " + nameFileConfig + "  \n")
                self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfig + "' >> log.txt \n")
                self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfig + " >> log.txt \n")
    
                for row in inFileConfig:
                    row = row.strip()
                    if row.startswith("global-nome_do_grupo"):
                        outFileConfig.write("global-nome_do_grupo = " + period + " - " + str(node.nameDesc) + " - Linha de pesquisa: " + lp +" - "+ self.dirUniversity.upper() + "\n")
                    elif row.startswith("global-arquivo_de_entrada"):
                        outFileConfig.write("global-arquivo_de_entrada = " + self.dirPath + "config/"+period+"/" + nameLP + ".list \n")    
                    elif row.startswith("global-diretorio_de_saida"):    
                        outFileConfig.write("global-diretorio_de_saida = " + self.dirPath + period + "/" + nameLP  +" \n")
                    elif row.startswith("global-email_do_admin"):    
                        outFileConfig.write("global-email_do_admin = " + email + "\n")
                    elif row.startswith("global-prefixo"):    
                        outFileConfig.write("global-prefixo = LP-"+period+ "-" + removerCaracteresEspeciais(lp).replace(" ", "_") +  "\n")
                    elif row.startswith("global-arquivo_qualis_de_periodicos"):
                        if(str(node.qualisCSV).find("#") < 0):    
                            outFileConfig.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                        else:
                            outFileConfig.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n") 
                    elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                        outFileConfig.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                    elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                        outFileConfig.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                    else:    
                        outFileConfig.write(row + "\n")    
                inFileConfig.close()    
                outFileConfig.close()    
                
    
    def writeFilesByDatas(self, node, value, cvsProgramas, cvsCategorias):
        if(node.listCvLattesProfessor == []):
            return
        periods = []
        year = self.initialYear
        while year <= self.finalYear:
            periods = periods + [str(year)]
            year = year + 1    
        periods = periods + [str(self.initialYear) + "-" + str(self.finalYear)]


        for period in periods:            
            nameFileList = self.dirPath + "config/"+period+"/PR-" + removerCaracteresEspeciais(value) + ".list"
            nameFileConfig = self.dirPath + "config/"+period+"/PR-" + removerCaracteresEspeciais(value) + ".config"
            outFileList = open(nameFileList, "wb")
            node.email = self.emailResponsavel
            
            for cv in node.listCvLattesProfessor:  
                pInicial = cv.getPeriodoInicial()
                pFinal = cv.getPeriodoFinal()       
                categoria = ""
                if(node.isLeaf == 1):
                    i = 0
                    for p in cvsProgramas:
                        if cv.getField(p) == node.name:
                            pInicial = cv.getPeriodoInicialPorPrograma(cvsProgramas[i])
                            pFinal = cv.getPeriodoFinalPorPrograma(cvsProgramas[i])
                            if(cv.getField(cvsCategorias[i]) == 'P'):
                                categoria = "Permanente"
                            elif(cv.getField(cvsCategorias[i]) == 'C'):
                                categoria = "Colaborador"
                            else:
                                categoria = cv.getField(cvsCategorias[i])
                            break                    
                        i = i + 1    
                    
                if len(period) == 4:
                    if (not (pInicial <= period and period <= pFinal) ):
                        continue
                data =     None
                if len(period) == 4:     
                    data = period + "-" + period
                else:    
                    data = str(pInicial) + "-" + str(pFinal)
                
                outFileList.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ", "+data+" , "+ str(categoria) +" \n" )
    
                if( cv.getField("Resp_prog") == value ):
                    node.email = cv.getField("mail")
                
                    
            outFileList.close()
        
            inFileConfig = None
            data = ""
            if(node.isLeaf == 1):
                inFileConfig = open(self.modeloProgramaDatas, 'rb')
                self.stat.contProgramaDatas += 1
            else:
                inFileConfig = open(self.modeloProgramasDatas, 'rb')
                self.stat.contProgramasDatas += 1
                
            outFileConfig = open(nameFileConfig, "wb")
            
            self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfig + "' \n")
            self.fileScriptRun.write("./scriptLattes.py " + nameFileConfig + "  \n")
            self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfig + "' >> log.txt \n")
            self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfig + " >> log.txt \n")
            for row in inFileConfig:
                row = row.strip()
                if row.startswith("global-nome_do_grupo"):
                    outFileConfig.write("global-nome_do_grupo = " + period + " - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")
                elif row.startswith("global-arquivo_de_entrada"):
                    outFileConfig.write("global-arquivo_de_entrada = " + self.dirPath + "config/"+period+"/PR-" + removerCaracteresEspeciais(value) + ".list \n")    
                elif row.startswith("global-diretorio_de_saida"):    
                    outFileConfig.write("global-diretorio_de_saida = " + self.dirPath + period + "/PR-" + removerCaracteresEspeciais(value)  +" \n")
                elif row.startswith("global-email_do_admin"):    
                    outFileConfig.write("global-email_do_admin = " + str(node.email) + "\n")
                elif row.startswith("global-prefixo"):    
                    outFileConfig.write("global-prefixo = PR-"+period+ "-" + removerCaracteresEspeciais(node.name) + "\n")
                elif row.startswith("global-arquivo_qualis_de_periodicos"): 
                    if(str(node.qualisCSV).find("#") < 0):    
                        outFileConfig.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                    else:
                        outFileConfig.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n") 
                elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                    outFileConfig.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                    outFileConfig.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                else:    
                    outFileConfig.write(row + "\n")    
            inFileConfig.close()    
            outFileConfig.close()    
            
    def writeFilesByPrograma(self, node, value):
        if(len(node.listCvLattesProfessor) == 0):
            return
            
        
        nameFileDir = self.dirPath + "config/PR-" + removerCaracteresEspeciais(value)
        nameFileList = nameFileDir + ".list"
        nameFileConfig = nameFileDir + ".config"
        
        nameFileProfAlunoDir = self.dirPath + "config/PR-AL-" + removerCaracteresEspeciais(value)
        nameFileListProfAluno = nameFileProfAlunoDir + ".list"
        nameFileConfigProfAluno =  nameFileProfAlunoDir + ".config"        
    
    
        nameFileAlunoMPDir = None
        nameFileListAlunoMP = None
        nameFileConfigAlunoMP = None
    
        nameFileAlunoMDir = None
        nameFileListAlunoM = None
        nameFileConfigAlunoM = None
    
        nameFileListAlunoMD = None
        nameFileConfigAlunoMD = None
        nameFileAlunoMDDir = None
        
        nameFileListAlunoD = None
        nameFileConfigAlunoD = None
        nameFileAlunoDDir = None    
        
        if( len(node.listCvLattesAlunosMP) > 0):
            if value.startswith("MP"):
                nameFileAlunoMPDir = self.dirPath + "config/AL-" + removerCaracteresEspeciais(value)
            else:
                nameFileAlunoMPDir = self.dirPath + "config/AL-MP-" + removerCaracteresEspeciais(value)
            nameFileListAlunoMP = nameFileAlunoMPDir + ".list"
            nameFileConfigAlunoMP = nameFileAlunoMPDir + ".config"        
            
        
        if( len(node.listCvLattesAlunosM) > 0):
            nameFileAlunoMDir = self.dirPath + "config/AL-M-" + removerCaracteresEspeciais(value)
            nameFileListAlunoM = nameFileAlunoMDir + ".list"
            nameFileConfigAlunoM = nameFileAlunoMDir + ".config"   
               
                
        if( len(node.listCvLattesAlunosD) > 0):        
            nameFileAlunoDDir = self.dirPath + "config/AL-D-" + removerCaracteresEspeciais(value)
            nameFileListAlunoD = nameFileAlunoDDir + ".list"
            nameFileConfigAlunoD = nameFileAlunoDDir + ".config"        
        
        if( len(node.listCvLattesAlunosD) > 0 and len(node.listCvLattesAlunosM) > 0):        
            nameFileAlunoMDDir = self.dirPath + "config/AL-MD-" + removerCaracteresEspeciais(value) 
            nameFileListAlunoMD = nameFileAlunoMDDir + ".list"
            nameFileConfigAlunoMD = nameFileAlunoMDDir + ".config"        
        
        
        outFileList = open(nameFileList, "wb")
        outFileListProfAluno = open(nameFileListProfAluno, "wb")
        node.email = self.emailResponsavel
        for cv in node.listCvLattesProfessor:        
            if (cv.getPeriodoFinal() < str(self.finalYear)):
                continue
            outFileList.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ",,\n" )
            outFileListProfAluno.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ",, \n" )        
            if(cv.getField("Resp_prog") == value ):
                node.email = cv.getField("mail")
        for cv in node.getAllListCvLattesAluno():
            outFileListProfAluno.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ",  , " +  cv.getField("ANO_1") + "\n" )
    
        outFileList.close()
        outFileListProfAluno.close()    
    
        if( len(node.listCvLattesAlunosMP) > 0):    
            if(node.key != "1"):
                outFileListAlunoMP = open(nameFileListAlunoMP, "wb")
                for cv in node.listCvLattesAlunosMP:
                    outFileListAlunoMP.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ",  , " +  cv.getAno(node.name) + "\n" )
                outFileListAlunoMP.close()
    
        
        if( len(node.listCvLattesAlunosM) > 0):    
            if(node.key != "1"):
                outFileListAlunoM = open(nameFileListAlunoM, "wb")
                for cv in node.listCvLattesAlunosM:
                    outFileListAlunoM.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ",  , " +  cv.getAno(node.name) + "\n" )
                outFileListAlunoM.close()
    
        if( len(node.listCvLattesAlunosD) > 0):    
            if(node.key != "1"):
                outFileListAlunoD = open(nameFileListAlunoD, "wb")
                for cv in node.listCvLattesAlunosD:
                    outFileListAlunoD.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ",  , " +  cv.getAno(node.name) + "\n" )
                outFileListAlunoD.close()
    
        if( len(node.listCvLattesAlunosD) > 0 and len(node.listCvLattesAlunosM) > 0):            
            outFileListAlunoMD = open(nameFileListAlunoMD, "wb")
            for cv in node.getAllListCvLattesAluno():
                outFileListAlunoMD.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ",  , " +  cv.getAno(node.name) + "\n" )
            outFileListAlunoMD.close()
        
        
        inFileConfig = None
        if(node.isLeaf == 1):
            inFileConfig = open(self.modeloPrograma, "rb")
            self.stat.contPrograma += 1
        else:
            inFileConfig = open(self.modeloProgramas, "rb")
            self.stat.contProgramas += 1
        
        outFileConfig = open(nameFileConfig, "wb")
        
        for row in inFileConfig:
            row = row.strip()
            if row.startswith("global-nome_do_grupo"):
                if(self.typeScript == "Professor"):
                    outFileConfig.write("global-nome_do_grupo = Professores - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")
                else:
                    outFileConfig.write("global-nome_do_grupo = " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")
            elif row.startswith("global-arquivo_de_entrada"):
                outFileConfig.write("global-arquivo_de_entrada = " + nameFileDir + ".list \n")    
            elif row.startswith("global-diretorio_de_saida"):    
                outFileConfig.write("global-diretorio_de_saida = " + self.dirPath + "PR-" + removerCaracteresEspeciais(value)  +" \n")
            elif row.startswith("global-email_do_admin"):    
                outFileConfig.write("global-email_do_admin = " + str(node.email) + "\n")
            elif row.startswith("global-prefixo"):    
                outFileConfig.write("global-prefixo = PR-" + removerCaracteresEspeciais(node.name) +  "\n")
            elif row.startswith("global-arquivo_qualis_de_periodicos"):
                if(str(node.qualisCSV).find("#") < 0):    
                    outFileConfig.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                else:
                    outFileConfig.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
            elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                outFileConfig.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
            elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                outFileConfig.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
            else:    
                outFileConfig.write(row + "\n")    
        inFileConfig.close()    
        outFileConfig.close()    
        
        self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfig + "' \n")
        self.fileScriptRun.write("./scriptLattes.py " + nameFileConfig + "  \n")
        self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfig + "' >> log.txt \n")
        self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfig + " >> log.txt \n")
        
        inFileConfigProfAluno = None
        if(node.isLeaf == 1):
            inFileConfigProfAluno = open(self.modeloProgramaComAlunos, "rb")
            self.stat.contProgramaComAlunos += 1
        else:
            inFileConfigProfAluno = open(self.modeloProgramasComAlunos, "rb")    
            self.stat.contProgramasComAlunos += 1
        
        outFileConfigProfAluno = open(nameFileConfigProfAluno, "wb")    
        for row in inFileConfigProfAluno:
            row = row.strip()
            if row.startswith("global-nome_do_grupo"):
                if( len(node.listCvLattesAlunosD) > 0 or len(node.listCvLattesAlunosM) > 0 or len(node.listCvLattesAlunosMP) > 0):
                    if(self.typeScript == "Professor"):
                        outFileConfigProfAluno.write("global-nome_do_grupo = Professores e Alunos - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")
                    else:
                        outFileConfigProfAluno.write("global-nome_do_grupo = " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")
                else:
                    outFileConfigProfAluno.write("global-nome_do_grupo = " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")    
            elif row.startswith("global-arquivo_de_entrada"):
                outFileConfigProfAluno.write("global-arquivo_de_entrada = " + nameFileProfAlunoDir + ".list \n")    
            elif row.startswith("global-diretorio_de_saida"):    
                outFileConfigProfAluno.write("global-diretorio_de_saida = " + self.dirPath + "PR-AL-" + removerCaracteresEspeciais(value) +" \n")
            elif row.startswith("global-email_do_admin"):    
                outFileConfigProfAluno.write("global-email_do_admin = " + str(node.email) + "\n")
            elif row.startswith("global-prefixo"):    
                outFileConfigProfAluno.write("global-prefixo = PR-AL-" + removerCaracteresEspeciais(node.name) +  "\n")
            elif row.startswith("global-arquivo_qualis_de_periodicos"):  
                if(str(node.qualisCSV).find("#") < 0):    
                    outFileConfigProfAluno.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                else:
                    outFileConfigProfAluno.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")  
            elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                outFileConfigProfAluno.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
            elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                outFileConfigProfAluno.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
            else:    
                outFileConfigProfAluno.write(row + "\n")    
        inFileConfigProfAluno.close()    
        outFileConfigProfAluno.close()    
        
        self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfigProfAluno + "' \n")
        self.fileScriptRun.write("./scriptLattes.py " + nameFileConfigProfAluno + "  \n")
    
        self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfigProfAluno + "' >> log.txt \n")
        self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfigProfAluno + " >> log.txt \n")
        
        
        if( len(node.listCvLattesAlunosMP) > 0):        
            if(node.key != "1"):
                inFileConfigAlunoMP = open(self.modeloAluno, "rb")
                self.stat.contAluno += 1
                outFileConfigAlunoMP = open(nameFileConfigAlunoMP, "wb")    
                for row in inFileConfigAlunoMP:
                    row = row.strip()
                    if row.startswith("global-nome_do_grupo"):
                        if ( node.isLeaf == 1):
                            outFileConfigAlunoMP.write("global-nome_do_grupo = Alunos de mestrado - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")
                        else:
                            outFileConfigAlunoMP.write("global-nome_do_grupo = Alunos de mestrado - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")    
                    elif row.startswith("global-arquivo_de_entrada"):
                        outFileConfigAlunoMP.write("global-arquivo_de_entrada = " + nameFileAlunoMPDir + ".list \n")    
                    elif row.startswith("global-diretorio_de_saida"):
                        if value.startswith("MP"):
                            outFileConfigAlunoMP.write("global-diretorio_de_saida = " + self.dirPath + "AL-" + removerCaracteresEspeciais(value)  +" \n")
                        else:
                            outFileConfigAlunoMP.write("global-diretorio_de_saida = " + self.dirPath + "AL-M-" + removerCaracteresEspeciais(value)  +" \n")
                    elif row.startswith("global-email_do_admin"):    
                        outFileConfigAlunoMP.write("global-email_do_admin = " + str(node.email) + "\n")
                    elif row.startswith("global-prefixo"):    
                        outFileConfigAlunoMP.write("global-prefixo = AL-M-" + removerCaracteresEspeciais(node.name) +  "\n")
                    elif row.startswith("global-arquivo_qualis_de_periodicos"):    
                        if(str(node.qualisCSV).find("#") < 0):    
                            outFileConfigAlunoMP.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                        else:
                            outFileConfigAlunoMP.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")  

                    elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                        outFileConfigAlunoMP.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                    elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                        outFileConfigAlunoMP.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                    else:    
                        outFileConfigAlunoMP.write(row + "\n")    
                inFileConfigAlunoMP.close()    
                outFileConfigAlunoMP.close()    
            
                self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoMP + "' \n")
                self.fileScriptRun.write("./scriptLattes.py " + nameFileConfigAlunoMP + "  \n")
    
                self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoMP + "' >> log.txt \n")
                self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfigAlunoMP + " >> log.txt \n")
        
        
        if( len(node.listCvLattesAlunosM) > 0):        
            if(node.key != "1"):
                inFileConfigAlunoM = open(self.modeloAluno, "rb")
                self.stat.contAluno += 1
                outFileConfigAlunoM = open(nameFileConfigAlunoM, "wb")    
                for row in inFileConfigAlunoM:
                    row = row.strip()
                    if row.startswith("global-nome_do_grupo"):
                        if ( node.isLeaf == 1):
                            outFileConfigAlunoM.write("global-nome_do_grupo = Alunos de mestrado - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")
                        else:
                            outFileConfigAlunoM.write("global-nome_do_grupo = Alunos de mestrado - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")    
                    elif row.startswith("global-arquivo_de_entrada"):
                        outFileConfigAlunoM.write("global-arquivo_de_entrada = " + nameFileAlunoMDir + ".list \n")    
                    elif row.startswith("global-diretorio_de_saida"):
                        if value.startswith("MP"):
                            outFileConfigAlunoM.write("global-diretorio_de_saida = " + self.dirPath + "AL-" + removerCaracteresEspeciais(value)  +" \n")
                        else:
                            outFileConfigAlunoM.write("global-diretorio_de_saida = " + self.dirPath + "AL-M-" + removerCaracteresEspeciais(value)  +" \n")
                    elif row.startswith("global-email_do_admin"):    
                        outFileConfigAlunoM.write("global-email_do_admin = " + str(node.email) + "\n")
                    elif row.startswith("global-prefixo"):    
                        outFileConfigAlunoM.write("global-prefixo = AL-M-" + removerCaracteresEspeciais(node.name) +  "\n")
                    elif row.startswith("global-arquivo_qualis_de_periodicos"): 
                        if(str(node.qualisCSV).find("#") < 0):    
                            outFileConfigAlunoM.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                        else:
                            outFileConfigAlunoM.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")  

                    elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                        outFileConfigAlunoM.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                    elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                        outFileConfigAlunoM.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                    else:    
                        outFileConfigAlunoM.write(row + "\n")    
                inFileConfigAlunoM.close()    
                outFileConfigAlunoM.close()    
            
                self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoM + "' \n")
                self.fileScriptRun.write("./scriptLattes.py " + nameFileConfigAlunoM + "  \n")
    
                self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoM + "' >> log.txt \n")
                self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfigAlunoM + " >> log.txt \n")
        
    
        if( len(node.listCvLattesAlunosD) > 0):        
            if(node.key != "1"):
                inFileConfigAlunoD = open(self.modeloAluno, "rb")
                self.stat.contAluno += 1
                outFileConfigAlunoD = open(nameFileConfigAlunoD, "wb")    
                for row in inFileConfigAlunoD:
                    row = row.strip()
                    if row.startswith("global-nome_do_grupo"):
                        outFileConfigAlunoD.write("global-nome_do_grupo = Alunos de doutorado - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")    
                    elif row.startswith("global-arquivo_de_entrada"):
                        outFileConfigAlunoD.write("global-arquivo_de_entrada = " + nameFileAlunoDDir + ".list \n")    
                    elif row.startswith("global-diretorio_de_saida"):    
                        outFileConfigAlunoD.write("global-diretorio_de_saida = " + self.dirPath + "AL-D-" + removerCaracteresEspeciais(value)  +" \n")
                    elif row.startswith("global-email_do_admin"):    
                        outFileConfigAlunoD.write("global-email_do_admin = " + str(node.email) + "\n")
                    elif row.startswith("global-prefixo"):    
                        outFileConfigAlunoD.write("global-prefixo = AL-D-" + removerCaracteresEspeciais(node.name) +  "\n")
                    elif row.startswith("global-arquivo_qualis_de_periodicos"):   
                        if(str(node.qualisCSV).find("#") < 0):    
                            outFileConfigAlunoD.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                        else:
                            outFileConfigAlunoD.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n") 
                    elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                        outFileConfigAlunoD.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                    elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                        outFileConfigAlunoD.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                    else:    
                        outFileConfigAlunoD.write(row + "\n")    
                inFileConfigAlunoD.close()    
                outFileConfigAlunoD.close()    
        
                self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoD + "' \n")
                self.fileScriptRun.write("./scriptLattes.py " + nameFileConfigAlunoD + "  \n")
    
                self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoD + "' >> log.txt \n")
                self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfigAlunoD + " >> log.txt \n")
    
            
        if( len(node.listCvLattesAlunosD) > 0 and len(node.listCvLattesAlunosM) > 0):            
            inFileConfigAlunoMD = open(self.modeloAluno, "rb")
            self.stat.contAluno += 1
            outFileConfigAlunoMD = open(nameFileConfigAlunoMD, "wb")    
            for row in inFileConfigAlunoMD:
                row = row.strip()
                if row.startswith("global-nome_do_grupo"):
                    outFileConfigAlunoMD.write("global-nome_do_grupo = Alunos de pós-graduações - " + str(node.nameDesc) +" - "+ self.dirUniversity.upper() + "\n")    
                elif row.startswith("global-arquivo_de_entrada"):
                    outFileConfigAlunoMD.write("global-arquivo_de_entrada = " + nameFileAlunoMDDir + ".list \n")    
                elif row.startswith("global-diretorio_de_saida"):    
                    outFileConfigAlunoMD.write("global-diretorio_de_saida = " + self.dirPath + "AL-MD-" + removerCaracteresEspeciais(value)  +" \n")
                elif row.startswith("global-email_do_admin"):    
                    outFileConfigAlunoMD.write("global-email_do_admin = " + str(node.email) + "\n")
                elif row.startswith("global-prefixo"):    
                    outFileConfigAlunoMD.write("global-prefixo = AL-MD-" + removerCaracteresEspeciais(node.name) +  "\n")
                elif row.startswith("global-arquivo_qualis_de_periodicos"):    
                    if(str(node.qualisCSV).find("#") < 0):    
                        outFileConfigAlunoMD.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")
                    else:
                        outFileConfigAlunoMD.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(node.qualisCSV) + "\n")  
                elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                    outFileConfigAlunoMD.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                    outFileConfigAlunoMD.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                else:    
                    outFileConfigAlunoMD.write(row + "\n")    
            inFileConfigAlunoMD.close()    
            outFileConfigAlunoMD.close()    
        
            self.fileScriptRun.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoMD + "' \n")
            self.fileScriptRun.write("./scriptLattes.py " + nameFileConfigAlunoMD + "  \n")
            
            self.fileScriptRunLog.write("echo 'processando ./scriptLattes.py " + nameFileConfigAlunoMD + "' >> log.txt \n")
            self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfigAlunoMD + " >> log.txt \n")
            
        
            
    def writeFilesByCV(self):
        listCvLattesProfessor = self.tree.root.listCvLattesProfessor
        periods = []
        year = self.initialYear
        while year <= self.finalYear:
            periods = periods + [str(year)]
            year = year + 1    
        periods = periods + [str(self.initialYear) + "-" + str(self.finalYear)]
        
        for pag in self.htmlPaginaPeridos:
            pag.write("<h2>Currículos: </h2>")
                
            
        index = -1    
        for period in periods:    
            index = index + 1  
            if len(self.tree.listNodeByQualis) > 1:
                self.htmlPaginaPeridos[index].write("<table border='1' style='border-collapse: collapse'><tr><td><center><b>Nome do professor</b></center></td><td colspan='"+ str(len(self.tree.listNodeByQualis)) +"'> &nbsp;<center><b>Qualis do programa que o professor pertence</b></center> &nbsp;</td></tr>")
                self.htmlPaginaPeridos[index].write("<tr><td>&nbsp;</td>")
                for nodeQualis in self.tree.listNodeByQualis:
                    self.htmlPaginaPeridos[index].write("<td><center><b>" + nodeQualis.name + "</center></b></td>")
                self.htmlPaginaPeridos[index].write("</tr>")    
            listCvLattesProfessor.sort(key=lambda a: a.getField("NOME"))
            for cv in listCvLattesProfessor:
                if len(period) == 4:
                    if (not (cv.getPeriodoInicial() <= period and period <= cv.getPeriodoFinal()) ):
                        continue
                
                if len(self.tree.listNodeByQualis) > 1:
                    self.htmlPaginaPeridos[index].write("<tr><td>"+ cv.getField("NOME") +"</td>")
                    
                if(self.tree.listNodeByQualis == []):
                    dirSaida =  self.dirPath + period + "/" + cv.getField("ID_Lattes")
                    nameFileList = self.dirPath + "config/" + period + "/" + cv.getField("ID_Lattes")  + ".list"  
                    nameFileConfig = self.dirPath + "config/" + period + "/" + cv.getField("ID_Lattes")  + ".config"
                    self.htmlPaginaPeridos[index].write("<a href='"+ cv.getField("ID_Lattes") + "/index.html' target='_blank'>"+ cv.getField("NOME")    +"</a><br/>")
                    
                    outFileList = open(nameFileList, "wb")
                        
                    data = None
                    if len(period) == 4:     
                        data = period + "-" + period
                    else:    
                        data = cv.getPeriodoInicial() + "-" + cv.getPeriodoFinal()
                        
                    outFileList.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ", "+data+",  \n" )
                    outFileList.close()
                            
                    inFileConfig = open(self.modeloProfessor, 'rb')
                    self.stat.contProfessor += 1
                    outFileConfig = open(nameFileConfig, "wb")
                    for row in inFileConfig:
                        row = row.strip()
                        if row.startswith("global-nome_do_grupo"):
                            if len(period) == 4:
                                outFileConfig.write("global-nome_do_grupo = " + cv.getField("NOME") + ", " + period +"\n")
                            else:
                                outFileConfig.write("global-nome_do_grupo = " + cv.getField("NOME") + ", " + data +"\n")
                        elif row.startswith("global-arquivo_de_entrada"):
                            outFileConfig.write("global-arquivo_de_entrada = " + nameFileList + "\n")    
                        elif row.startswith("global-diretorio_de_saida"):    
                            outFileConfig.write("global-diretorio_de_saida = " + dirSaida  +" \n")
                        elif row.startswith("global-email_do_admin"):    
                            outFileConfig.write("global-email_do_admin = " + cv.getField("mail") + "\n")
                        elif row.startswith("global-prefixo"):    
                            outFileConfig.write("global-prefixo = " + cv.getField("ID_Lattes") + "\n")
                        elif row.startswith("global-arquivo_qualis_de_periodicos"):    
                            outFileConfig.write("# global-arquivo_qualis_de_periodicos = \n") 
                        elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                            outFileConfig.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                        elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                            outFileConfig.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                        else:    
                            outFileConfig.write(row + "\n")    
                    inFileConfig.close()    
                    outFileConfig.close()    
                    self.fileScriptRun.write("echo '"+ cv.getField("ID_Lattes") +" - "+ cv.getField("NOME") +" - processando ./scriptLattes.py " + nameFileConfig + "' \n")
                    self.fileScriptRun.write("./scriptLattes.py " + nameFileConfig + "  \n")
                    self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfig + " >> log.txt \n")
                    self.fileScriptRunLog.write("echo '"+ cv.getField("ID_Lattes") +" - "+ cv.getField("NOME") +" => processando ./scriptLattes.py " + nameFileConfig + "' >> log.txt \n")    
                
                       
                       
                       
                        
                for nodeQualis in self.tree.listNodeByQualis:
                    if len(self.tree.listNodeByQualis) > 1:
                        if cv in nodeQualis.listCvLattesProfessor:
                            dirSaida =  self.dirPath + period + "/" + cv.getField("ID_Lattes") + "_" + removerCaracteresEspeciais(nodeQualis.name) 
                            nameFileList = self.dirPath + "config/" + period + "/" + cv.getField("ID_Lattes")  + "_" + removerCaracteresEspeciais(nodeQualis.name) + ".list"  
                            nameFileConfig = self.dirPath + "config/" + period + "/" + cv.getField("ID_Lattes") + "_" + removerCaracteresEspeciais(nodeQualis.name) + ".config"
                            #self.htmlPaginaPeridos[index].write("<a href='"+ cv.getField("ID_Lattes") + "_" + removerCaracteresEspeciais(nodeQualis.name) + "/index.html' target='_blank'>"+ cv.getField("NOME")  +"</a><br/>")
                            self.htmlPaginaPeridos[index].write("<td> <a href='"+ cv.getField("ID_Lattes") + "_" + removerCaracteresEspeciais(nodeQualis.name) + "/index.html' target='_blank'><center>"+ nodeQualis.areaQualis  +"</center></a> </td>")
                        else:
                            self.htmlPaginaPeridos[index].write("<td> &nbsp; </td>")
                            continue        
                    else:
                        dirSaida =  self.dirPath + period + "/" + cv.getField("ID_Lattes")
                        nameFileList = self.dirPath + "config/" + period + "/" + cv.getField("ID_Lattes")  + ".list"  
                        nameFileConfig = self.dirPath + "config/" + period + "/" + cv.getField("ID_Lattes")  + ".config"
                        self.htmlPaginaPeridos[index].write("<a href='"+ cv.getField("ID_Lattes") + "/index.html' target='_blank'>"+ cv.getField("NOME")    +"</a><br/>")
                    
                    outFileList = open(nameFileList, "wb")
                    
                    
                        
                    data = None
                    if len(period) == 4:     
                        data = period + "-" + period
                    else:    
                        data = cv.getPeriodoInicial() + "-" + cv.getPeriodoFinal()
                        
                    outFileList.write(cv.getField("ID_Lattes") + "," + cv.getField("NOME") + ", "+data+",  \n" )
                    outFileList.close()
                            
                    inFileConfig = open(self.modeloProfessor, 'rb')
                    self.stat.contProfessor += 1
                    outFileConfig = open(nameFileConfig, "wb")
                    for row in inFileConfig:
                        row = row.strip()
                        if row.startswith("global-nome_do_grupo"):
                            if len(period) == 4:
                                outFileConfig.write("global-nome_do_grupo = " + cv.getField("NOME") + ", " + period +"\n")
                            else:
                                outFileConfig.write("global-nome_do_grupo = " + cv.getField("NOME") + ", " + data +"\n")
                        elif row.startswith("global-arquivo_de_entrada"):
                            outFileConfig.write("global-arquivo_de_entrada = " + nameFileList + "\n")    
                        elif row.startswith("global-diretorio_de_saida"):    
                            outFileConfig.write("global-diretorio_de_saida = " + dirSaida  +" \n")
                        elif row.startswith("global-email_do_admin"):    
                            outFileConfig.write("global-email_do_admin = " + cv.getField("mail") + "\n")
                        elif row.startswith("global-prefixo"):    
                            outFileConfig.write("global-prefixo = " + cv.getField("ID_Lattes") + "\n")
                        elif row.startswith("global-arquivo_qualis_de_periodicos"):    
                            if(str(nodeQualis.qualisCSV).find("#") < 0):    
                                outFileConfig.write("global-arquivo_qualis_de_periodicos = " + self.dirPath + str(nodeQualis.qualisCSV) + "\n")
                            else:
                                outFileConfig.write("# global-arquivo_qualis_de_periodicos = " + self.dirPath + str(nodeQualis.qualisCSV) + "\n") 
                        elif row.startswith("global-diretorio_de_armazenamento_de_cvs"):    
                            outFileConfig.write("global-diretorio_de_armazenamento_de_cvs = " + self.dirCache + "\n")
                        elif row.startswith("global-diretorio_de_armazenamento_de_doi"):    
                            outFileConfig.write("global-diretorio_de_armazenamento_de_doi = " + self.dirDOI + "\n")
                        else:    
                            outFileConfig.write(row + "\n")    
                    inFileConfig.close()    
                    outFileConfig.close()    
                    self.fileScriptRun.write("echo '"+ cv.getField("ID_Lattes") +" - "+ cv.getField("NOME") +" - processando ./scriptLattes.py " + nameFileConfig + "' \n")
                    self.fileScriptRun.write("./scriptLattes.py " + nameFileConfig + "  \n")
                    self.fileScriptRunLog.write("./scriptLattes.py " + nameFileConfig + " >> log.txt \n")
                    self.fileScriptRunLog.write("echo '"+ cv.getField("ID_Lattes") +" - "+ cv.getField("NOME") +" => processando ./scriptLattes.py " + nameFileConfig + "' >> log.txt \n")    
                
                if len(self.tree.listNodeByQualis) > 1:
                    self.htmlPaginaPeridos[index].write("</tr>")
            if len(self.tree.listNodeByQualis) > 1:
                self.htmlPaginaPeridos[index].write("</table>")
                               
    def printEstatistica(self):
        print "\n====================================================================="            
        print "\nQuantidade de config/list do scriptlattes: " + str(self.stat.getTotal())
        print "Quantidade de CVs de alunos: " + str(len(self.tree.root.listCvLattesAlunos))
        print "Quantidade de CVs de professores: " + str(len(self.tree.root.listCvLattesProfessor))
        print "Total de CVs: " + str(len(self.tree.root.listCvLattesProfessor) + len(self.tree.root.listCvLattesAlunos))
        print "\n====================================================================="    
        print "\nEstatistica de templates (config/list) utilizados"
        print "modelo_aluno: " + str(self.stat.contAluno)
        print "modelo_professor: " + str(self.stat.contProfessor)
        print "modelo_linha_pesquisa: " + str(self.stat.contLinhaPesquisa)
        print "modelo_programa_com_alunos: " + str(self.stat.contProgramaComAlunos)
        print "modelo_programas_com_alunos: " + str(self.stat.contProgramasComAlunos)
        print "modelo_programa: " + str(self.stat.contPrograma)
        print "modelo_programas: " + str(self.stat.contProgramas)
        print "modelo_programa_datas: " + str(self.stat.contProgramaDatas)
        print "modelo_programas_datas: " + str(self.stat.contProgramasDatas)
        print "====================================================================="
        
    def loadDirPath(self, pathComp):
        with open(pathComp, 'rb') as compFile:
            for row in compFile:
                row = row.strip()
                if row.startswith("Universidade_Abreviado"):
                    self.dirUniversity = row.split(':')[1].strip()
                    self.dirPath = self.dirPath + removerCaracteresEspeciais(self.dirUniversity) + "/"
                    if not os.path.exists(self.dirPath):
                        os.makedirs(self.dirPath)
                if row.startswith("Programa_Abreviado_1"):
                    #key = row.split(':')[0].strip()
                    value = row.split(':')[1].strip()
                    self.dirPath = self.dirPath + removerCaracteresEspeciais(value) + "/"
                    if not os.path.exists(self.dirPath):
                        os.makedirs(self.dirPath)
                    return
               
        
    def execute(self, pathComp, cvsProgramas, cvsCategorias, cvsLinhasPesquisas, cvsRespLP):
        with open(pathComp, 'rb') as compFile:
            for row in compFile:
                row = row.strip()
                if row.startswith("Universidade_Completo"):        
                    nameUniversity = row.split(':')[1].strip()
                if row.startswith("Programa_") :
                    key = row.split(':')[0].strip()
                    value = row.split(':')[1].strip()
    
                    node = self.tree.mapsNodes[key[key.rfind("_")+1: len(key)]]
                    
                    if key == "Programa_Abreviado_1":
                        #self.dirPath = self.dirPath + removerCaracteresEspeciais(value) + "/"
                        if not os.path.exists(self.dirPath):
                            os.makedirs(self.dirPath)                
                        if not os.path.exists(self.dirPath + "config"):
                            os.makedirs(self.dirPath + "config")                    
                        self.fileScriptRun = open(self.dirPath + "config/"+ removerCaracteresEspeciais(value) +".sh", "wb")
                        self.fileScriptRunLog = open(self.dirPath + "config/"+ removerCaracteresEspeciais(value) +"_LOG.sh", "wb")                    
                        htmlPaginaRaiz = open(self.dirPath + "index.html", "wb")
                        htmlPaginaRaiz.write("<html><head><meta content='text/html; charset=utf-8' http-equiv='content-type'><title>"+self.titleHtml+ " " + nameUniversity +"</title></head><body>\n")
                        self.fileScriptRun.write("echo 'inicializando...' \n")
                        self.fileScriptRunLog.write("echo 'inicializando [processamento com log]...' \n")
                        self.fileScriptRunLog.write("echo 'inicializando...' > log.txt \n")
                        year = self.initialYear
                        while year <= self.finalYear:
                            if not os.path.exists(self.dirPath + str(year)):
                                os.makedirs(self.dirPath + str(year))                
                            if not os.path.exists(self.dirPath + "config/"+ str(year)):
                                os.makedirs(self.dirPath + "config/"+ str(year))                
                            self.htmlPaginaPeridos = self.htmlPaginaPeridos + [open(self.dirPath + str(year) + "/index.html", "wb")]
                            self.mapPeridos = self.mapPeridos + [str(year)]
                            self.htmlPaginaPeridos[len(self.htmlPaginaPeridos)-1].write("<html><head><meta content='text/html; charset=utf-8' http-equiv='content-type'><title>"+ str(year) + "- " +node.nameDesc +"</title></head><body>\n")
                            self.htmlPaginaPeridos[len(self.htmlPaginaPeridos)-1].write("<h1>Período "+ str(year) +"</h1>\n")                        
                            year = year + 1    
                        if not os.path.exists(self.dirPath + str(self.initialYear) + "-" + str(self.finalYear)):
                            os.makedirs(self.dirPath + str(self.initialYear) + "-" + str(self.finalYear))                
                        if not os.path.exists(self.dirPath + "config/"+ str(self.initialYear) + "-" + str(self.finalYear)):
                            os.makedirs(self.dirPath + "config/"+ str(self.initialYear) + "-" + str(self.finalYear))                        
                        self.htmlPaginaPeridos = self.htmlPaginaPeridos + [open(self.dirPath + str(self.initialYear) + "-" + str(self.finalYear) + "/index.html", "wb")]
                        self.mapPeridos = self.mapPeridos + [str(self.initialYear) + "-" + str(self.finalYear)]                    
                        self.htmlPaginaPeridos[len(self.htmlPaginaPeridos)-1].write("<html><head><meta content='text/html; charset=utf-8' http-equiv='content-type'><title>"+ str(self.initialYear) + "-" + str(self.finalYear) + " - "+ node.nameDesc +"</title></head><body>\n")
                        self.htmlPaginaPeridos[len(self.htmlPaginaPeridos)-1].write("<h1>Período "+ str(self.initialYear) + "-" + str(self.finalYear) +"</h1>\n")
                        
    
                        
                    if key.startswith("Programa_Abreviado_"):
                        key = key[key.rfind("_")+1: len(key)]
                        #node = tree.mapsNodes[key]
                        
                        if key == "1":
                            if self.tree.getNumNode() <= 2:
                                htmlPaginaRaiz.write("<h"+str(len(key))+">"+ node.nameDesc+  " da "+ nameUniversity +"</h"+str(len(key))+">\n")
                            else:
                                htmlPaginaRaiz.write("<h"+str(len(key))+"><a target='Branco' href='PR-AL-" + removerCaracteresEspeciais(value) + "/index.html'>"+ node.nameDesc+  "</a> da "+ nameUniversity +"</h"+str(len(key))+">\n") 
                        else: 
                            if(len(node.listCvLattesProfessor) > 0 and len(node.listCvLattesAlunos) > 0):               
                                htmlPaginaRaiz.write(("<ul>" * (len(key)-1) )+"<li><h"+str(len(key))+"><a target='Branco' href='PR-AL-" + removerCaracteresEspeciais(value) + "/index.html'>"+ node.nameDesc+  "</a></h"+str(len(key))+"></li>\n")    
                            else:
                                if(len(node.listCvLattesProfessor) > 0 and len(node.listCvLattesAlunos) == 0):
                                    htmlPaginaRaiz.write(("<ul>" * (len(key)-1) )+"<li><h"+str(len(key))+"><a target='Branco' href='PR-" + removerCaracteresEspeciais(value) + "/index.html'>"+ node.nameDesc+  "</a></h"+str(len(key))+"></li>\n")
                                else:                                    
                                    htmlPaginaRaiz.write(("<ul>" * (len(key)-1) )+"<li><h"+str(len(key))+">"+ node.nameDesc+  "</h"+str(len(key))+"></li>\n")
                        if  (key == "1"):
                            if(self.tree.getNumNode() > 2):
                                if(gerador.typeScript == "Professor"):
                                    htmlPaginaRaiz.write("<ul><li><a target='Branco' href='PR-" + removerCaracteresEspeciais(value) + "/index.html'>Professores</a></li>\n")
                                else:
                                    htmlPaginaRaiz.write("<ul><li><a target='Branco' href='PR-" + removerCaracteresEspeciais(value) + "/index.html'>Egressos</a></li>\n") 
                        else:
                            if( not(len(node.listCvLattesProfessor) > 0 and len(node.listCvLattesAlunos) == 0) ):
                                htmlPaginaRaiz.write("<ul><li><a target='Branco' href='PR-" + removerCaracteresEspeciais(value) + "/index.html'>Professores</a></li>\n")
                        if  (key == "1" and (len(node.listCvLattesAlunosD) > 0 or len(node.listCvLattesAlunosM) > 0 or len(node.listCvLattesAlunosMP) > 0)):
                            if(self.tree.getNumNode() > 2):
                                htmlPaginaRaiz.write("<li><a target='Branco' href='AL-MD-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos</a></li>\n")
                            elif(key == "1" and len(node.listCvLattesAlunosD) > 0 and len(node.listCvLattesAlunosM) > 0):
                                htmlPaginaRaiz.write("<li><a target='Branco' href='AL-MD-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos</a></li>\n")
                        elif (len(node.listCvLattesAlunosD) > 0 and len(node.listCvLattesAlunosM) > 0):
                            htmlPaginaRaiz.write("<li><a target='Branco' href='AL-MD-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos</a></li>\n")
                            htmlPaginaRaiz.write("<ul><li><a target='Branco' href='AL-D-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos de doutorado</a></li>\n")
                            htmlPaginaRaiz.write("<li><a target='Branco' href='AL-M-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos de mestrado</a></li>\n")
                            htmlPaginaRaiz.write("</ul>")
                        elif (len(node.listCvLattesAlunosD) > 0):
                            htmlPaginaRaiz.write("<li><a target='Branco' href='AL-D-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos de doutorado</a></li>\n")
                        elif (len(node.listCvLattesAlunosM) > 0):
                            htmlPaginaRaiz.write("<li><a target='Branco' href='AL-M-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos de mestrado</a></li>\n")
                        elif (len(node.listCvLattesAlunosMP) > 0):
                            htmlPaginaRaiz.write("<li><a target='Branco' href='AL-" + removerCaracteresEspeciais(value) + "/index.html'>Alunos de mestrado</a></li>\n")
                            
                        
                        htmlPaginaRaiz.write("</ul>" * len(key))
                        index = 0
                        
                        while index < len(self.htmlPaginaPeridos):
                            if(node.hasCvInPeriod(self.mapPeridos[index]) or index == (len(self.htmlPaginaPeridos)-1)):
                                pag = self.htmlPaginaPeridos[index]
                                #print str(index) + " => " + self.mapPeridos[index] + " ==>>" + str(node.hasCvInPeriod(self.mapPeridos[index]))
                                if node == self.tree.root:
                                    if(len(node.children) == 1):
                                        pag.write("<h"+str(len(key))+">"+node.nameDesc+"</h"+str(len(key))+">")
                                    else:
                                        pag.write("<h"+str(len(key))+"><a href='PR-"+ removerCaracteresEspeciais(value) +"/index.html' target='_blank'>xyz1"+node.nameDesc+"</a></h"+str(len(key))+">")
                                else:
                                    pag.write("<h"+str(len(key))+">"+ ("<ul>" * (len(key)-1))+ "<li><a href='PR-"+ removerCaracteresEspeciais(value) +"/index.html' target='_blank'>xyz2"+node.nameDesc+"</a></li>"+ ("</ul>" * (len(key)-1)) +"</h"+str(len(key))+">")    
                            index = index+1  
    
                        self.writeFilesByPrograma(node, value)
                        self.writeFilesByDatas(node, value, cvsProgramas, cvsCategorias)
                        self.writeFilesByLinhasPesquisas(node, value, cvsProgramas, cvsLinhasPesquisas, cvsRespLP)
    
                        for pag in self.htmlPaginaPeridos:
                            pag.write("</ul>")
    
        
        #for pag in self.htmlPaginaPeridos:
        #    pag.write("<h2>Por professor: </h2>")
        #for nodeQualis in self.tree.listNodeByQualis:
        self.writeFilesByCV()  
              
        self.fileScriptRun.close()
        self.fileScriptRunLog.close()    
        htmlPaginaRaiz.write("<br>Preparação para plataforma Sucupira: ")
        year = self.initialYear
        while year <= self.finalYear:
            htmlPaginaRaiz.write("<a href='"+ str(year) +"/index.html' target='_blank'>"+str(year)+"</a>, ")
            year = year + 1    
            
            
        htmlPaginaRaiz.write("<a href='"+ str(self.initialYear) + "-" + str(self.finalYear) +"/index.html' target='_blank'>Período "+str(self.initialYear) + "-" + str(self.finalYear)+"</a><br>")
        htmlPaginaRaiz.write("<a target='copyright' href='https://bitbucket.org/vlab4u'>ScriptSucupira</a>©, <a target='copyright' href='http://scriptlattes.sourceforge.net/'>ScriptLattes</a>©") 
        htmlPaginaRaiz.close()
        for pag in self.htmlPaginaPeridos:    
            if len(self.tree.listNodeByQualis) > 1:
                if(gerador.typeScript == "Professor"):
                    pag.write("<br>Os extratos da <a href='http://qualis.capes.gov.br/webqualis/publico/pesquisaPublicaClassificacao.seam;jsessionid=D72D57B5E1566D753A3C5B01C18EC2DD.qualismodcluster-node-64?conversationPropagation=begin' target='_blank'>WebQualis</a> usados:<ul>")
                    for node in self.tree.listNodeByQualis:
                        pag.write("<li>"+node.nameDesc  + "("+ node.name +") : " + node.areaQualis + " <a href='../"+ str(node.qualisPDF) +"' target='_blank'>em pdf</a>, <a href='../"+ str(node.qualisCSV) +"' target='_blank'>em csv</a></li>\n")
                pag.write("</ul><a target='copyright' href='https://bitbucket.org/vlab4u'>ScriptSucupira</a>©, <a target='copyright' href='http://scriptlattes.sourceforge.net/'>ScriptLattes</a>©<br/></body></html>")
            else:
                for node in self.tree.listNodeByQualis:
                    if(gerador.typeScript == "Professor"):
                        pag.write("<br>O extrato da <a href='http://qualis.capes.gov.br/webqualis/publico/pesquisaPublicaClassificacao.seam;jsessionid=D72D57B5E1566D753A3C5B01C18EC2DD.qualismodcluster-node-64?conversationPropagation=begin' target='_blank'>")
                        pag.write("WebQualis</a> usado, <a href='../"+ str(node.qualisPDF) +"' target='_blank'>em pdf</a>, <a href='../"+ str(node.qualisCSV) +"' target='_blank'>em csv</a>\n")
                pag.write("<br><a target='copyright' href='https://bitbucket.org/vlab4u'>ScriptSucupira</a>©, <a target='copyright' href='http://scriptlattes.sourceforge.net/'>ScriptLattes</a>©<br/></body></html>")
                
            pag.close()
                        

#----------------------------------------------------------------------------------------------------        
if __name__ == "__main__":
    
    pathCSV = None
    pathCsvAluno = None
    separadorCsv = ","
    pathComp = sys.argv[1]
    
    scriptColetaVersaoData = u"Versao 1.3 - disponibilizado em 12 de agosto de 2015"
    print "\n ScriptSucupira - " + scriptColetaVersaoData
    
    listLattesP = []
    listLattesA = []
    
    cvsProgramas = []
    cvsCategorias = []
    cvsLinhasPesquisas = []
    cvsRespLP = []
    
    gerador = GeradorScriptlattes() #@ivar: Create GeradorScriptlattes Object
    
    #carregar dos iniciais do Config
    with open(pathComp, 'rb') as compFile:
        for row in compFile:
            row = row.strip()
            if row.startswith("diretorio_de_armazenamento_de_cvs"):
                gerador.dirCache = row.split(':')[1].strip()
                #print "dirCache:" +dirCache
            elif row.startswith("diretorio_de_armazenamento_de_doi"):
                gerador.dirDOI = row.split(':')[1].strip()
                #print "dirDOI:" +dirDOI
            elif row.startswith("arquivo_qualis_de_periodicos_csv"):
                gerador.fileQualis = row.split(':')[1].strip()
                #print "fileQualis:" +fileQualis
            elif row.startswith("arquivo_qualis_de_periodicos_pdf"):
                gerador.fileQualisPDF = row.split(':')[1].strip()
                #print "fileQualisPDF:" +fileQualisPDF                
            elif row.startswith("email_responsavel_conjunto_cursos"):
                gerador.emailResponsavel = row.split(':')[1].strip()
                #print "emailResponsavel:" +emailResponsavel                
            elif row.startswith("modelo_aluno"):
                gerador.modeloAluno = row.split(':')[1].strip()
                #print "modeloAluno:" +modeloAluno                
            elif row.startswith("modelo_professor"):
                gerador.modeloProfessor = row.split(':')[1].strip()
                #print "modeloProfessor:" +modeloProfessor                
            elif row.startswith("modelo_linha_pesquisa"):
                gerador.modeloLinhaPesquisa = row.split(':')[1].strip()
                #print "modeloLinhaPesquisa:" +modeloLinhaPesquisa                
            elif row.startswith("modelo_programa_com_alunos"):
                gerador.modeloProgramaComAlunos = row.split(':')[1].strip()
                #print "modeloProgramaComAlunos:" +modeloProgramaComAlunos                
            elif row.startswith("modelo_programas_com_alunos"):
                gerador.modeloProgramasComAlunos = row.split(':')[1].strip()
                #print "modeloProgramasComAlunos:" +modeloProgramasComAlunos            
            elif row.startswith("modelo_programa_datas"):
                gerador.modeloProgramaDatas = row.split(':')[1].strip()
                #print "modeloProgramaDatas:" +modeloProgramaDatas            
            elif row.startswith("modelo_programas_datas"):
                gerador.modeloProgramasDatas = row.split(':')[1].strip()
                #print "modeloProgramasDatas:" +modeloProgramasDatas            
            elif row.startswith("modelo_programas"):
                gerador.modeloProgramas = row.split(':')[1].strip()
                #print "modeloProgramas:" +modeloProgramas            
            elif row.startswith("modelo_programa"):
                gerador.modeloPrograma = row.split(':')[1].strip()
                #print "modeloPrograma:" +modeloPrograma            
            elif row.startswith("dataset_professores"):
                pathCSV = row.split(':')[1].strip()
                #print "pathCSV:" +pathCSV            
            elif row.startswith("dataset_alunos"):
                pathCsvAluno = row.split(':')[1].strip()
                #print "pathCsvAluno:" +pathCsvAluno         
            elif row.startswith("separador_csv"):
                separadorCsv = row.split(':')[1].strip()   
                   
        compFile.close()
    
    #carrega os dados do CSV
    with open(pathCSV, 'rb') as csvfile:
        if("t" in separadorCsv and len(separadorCsv) == 2):
            reader = csv.DictReader(csvfile, fieldnames=[], restkey='undefined-fieldnames', delimiter='\t', quoting=csv.QUOTE_NONE)
        else:
            reader = csv.DictReader(csvfile, fieldnames=[], restkey='undefined-fieldnames', delimiter=separadorCsv, quoting=csv.QUOTE_NONE)
        current_row = 0
        for row in reader:
            current_row += 1
            if current_row == 1:
                reader.fieldnames = row['undefined-fieldnames']
            else:
                cv = CvLattesProfessor(row)
                gerador.initialYear = min(gerador.initialYear,     min(cv.periods))
                gerador.finalYear = max(gerador.finalYear,     max(cv.periods))    
                listLattesP = listLattesP + [cv]

        for programa in reader.fieldnames:
            if(programa.startswith("PROGRAMA_")):
                cvsProgramas = cvsProgramas + [programa]
            if(programa.startswith("CATEG_")):
                cvsCategorias = cvsCategorias + [programa]
            if(programa.startswith("LP_")):
                cvsLinhasPesquisas = cvsLinhasPesquisas + [programa]
            if(programa.startswith("Resp_LP")):
                cvsRespLP = cvsRespLP + [programa]            
        csvfile.close()  
                       
    listLattesA = []
    if(pathCsvAluno != None and len(pathCsvAluno) > 3):         
        with open(pathCsvAluno, 'rb') as csvfile:
            if("t" in separadorCsv and len(separadorCsv) == 2):
                reader = csv.DictReader(csvfile,  delimiter='\t', quoting=csv.QUOTE_NONE)
            else:
                reader = csv.DictReader(csvfile,  delimiter=separadorCsv, quoting=csv.QUOTE_NONE)
            for row in reader:
                cv = CvLattesAluno(row)
                listLattesA = listLattesA + [cv]
            csvfile.close()
    
    gerador.loadDirPath(pathComp)      
    gerador.tree = Tree(gerador.dirPath, pathComp, listLattesP, listLattesA, cvsProgramas)
    if(gerador.tree.listNodeByQualis == []):
        gerador.typeScript = "Egresso"
      
    gerador.tree.printTree()
    gerador.tree.drawTree()
    gerador.execute(pathComp, cvsProgramas, cvsCategorias, cvsLinhasPesquisas, cvsRespLP)
    gerador.printEstatistica()
        
    print "\nScriptSucupira executado com sucesso!\n\n"
    
