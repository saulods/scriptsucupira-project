# Parametros globais
# Cache para cumprir varias config scriptlattes apos baixar uma vez so os dados da web
diretorio_de_armazenamento_de_cvs : ./cache/uninove/administracao/cache 
diretorio_de_armazenamento_de_doi : ./cache/uninove/administracao/doi 

area_de_avaliacao_qualis: Administraçao
# este mail aparecera em baixo do levantamento scriptlattes (certificaçao)
# email de curriculos individuais serao recuperados na planilha de prof
# email de responsavel de cada programa e linha de pesquisa tambem
email_responsavel_conjunto_cursos : maccari@uninove.br

# Modelos de configs do scriptlattes para adequar a config scriptlattes conforme o conjunto analisado
# Estes modelos estarao usados para as gerar as config scriptlattes... 
# personalisando cada um destes modelos e possivel gerar uma grande gama de resultado final
# Modelo para conjunto de alunos
modelo_aluno : ./modelos/AL-Alunos.config
# Modelo para curriculo individual de professor (usado por periodo somente)
modelo_professor: ./modelos/CV_Prof.config
# modelo par linha de pesquisa (usado por periodo somente)
modelo_linha_pesquisa: ./modelos/LP-XXXX.config
# modelo para um programa conjunto professor e alunos
modelo_programa_com_alunos: ./modelos/PR-AL-Programa.config
# modelo para um conjunto de programmas analisando o conjunto professor-Alunos
modelo_programas_com_alunos: ./modelos/PR-AL-Programas.config
# Modelo para o conjunto de professores de um programa
modelo_programa: ./modelos/PR-Programa.config
# modelo para um conjuntos de programas, o conjunto de professores
modelo_programas: ./modelos/PR-Programas.config
# modelo para um programa analisado por periodo, conjunto de professores
modelo_programa_datas: ./modelos/PR-Programa-Datas.config
# Modelo para um conjunto de programa anlisado por periodo, o conjunto de profesores
modelo_programas_datas: ./modelos/PR-Programas-Datas.config

# Dataset (planilhas de professores e alunos - formato csv)
# cuidado respectar cabeçario e nome programa da descriçao do curso
# ID_Lattes	MATRICULA	NOME	CH	LP_1	LP_2	LP_3	PROGRAMA_1	CATEG_1	PROGRAMA_2	CATEG_2	PROGRAMA_3	CATEG_3	Periodo_Inicio	Periodo_Fim	mail	Resp_prog	Prog_linha	Resp_LP1	Resp_LP2	Resp_LP3	Resp_LP4
# a ordem das colunas ou a repetição (ou não) das categorias repetitivas nao imortam... mais os nomes das colunas nao pode ser alterado.
dataset_professores: U9_ADM-Professor.csv
# planilha alunos pode ser optional desativar com jogo da velha ao inicio da linha
dataset_alunos: U9_ADM-Aluno.csv
# separador das informaçoes nas planilhas pode ser tabulaçao (\t) virgula (,) ... o separador nao deve existir nos dados ou colocar aspas(")
separador_csv: \t

# Universidade
# Abreviado para os nomes dos directorios
Universidade_Abreviado : uninove
# Completo para escritura na pagina web
Universidade_Completo : Universidade Nove de Julho

# Descricao da hierarquia de programas
# prestar atençao a hierarquia. !!! Nome abreviado deve ser o mesmo na planilha dos prof e alunos !!!
# Abreviado para reconhecimento nas planilhas e nome dos directorios e arquivos
# completo para escritura na pagina em html
# o primeiro e recomandavel ser a area
# cada nome abreviado tem que ser differente
Programa_Abreviado_1 : administração
Programa_Completo_1 : Stricto Sensu em Administração
Programa_Alunos_1:
# Qualis a ser usada . O pdf e somente para transparencia na pagina publicada.
Programa_arquivo_qualis_de_periodicos_csv_1 : ./qualis/qualis_admin_journal_2012.csv
Programa_arquivo_qualis_de_periodicos_pdf_1 : ./qualis/qualis_admin_journal_2012.pdf
Programa_area_de_avaliacao_qualis_1: Administração

# A stricto sensu de ADM da Uninove se desdobra em academico e professionais
# o Academico se desdobra em mestrado e dotourado
Programa_Abreviado_11 : PPGA
Programa_Completo_11 : Programa de Pós-Graduação em Administração
Programa_Alunos_11 : D-PPGA,M-PPGA

Programa_Abreviado_12 : MPA
Programa_Completo_12 : Mestrados Profissionais em Administração
Programa_Alunos_12 :

# Os mestrados professionais se desdobra em especialidade
Programa_Abreviado_121 : MPA-GP
Programa_Completo_121 : Gestão de Projetos
Programa_Alunos_121 : MPA-GP
Programa_Abreviado_122 : MPA-GeAS
Programa_Completo_122 : Gestão Ambiental e Sustentabilidade
Programa_Alunos_122 : MPA-GeAS
Programa_Abreviado_123 : MPA-GE
Programa_Completo_123 : Gestão do Esporte
Programa_Alunos_123 : MPA-GE
Programa_Abreviado_124 : MPA-GS
Programa_Completo_124 : Gestão em Sistemas de Saúde
Programa_Alunos_124 : MPA-GS
